Al final d'aquest fitxer hi ha fragments de les zones del DNS de les vostres
aules.  Com que hi ha un RR per cada estació de treball els fitxers originals
tenen més de 200 línies cada un.

Això es por abreujar **molt** fent servir la directiva `$GENERATE` que _Bind_
ens proporciona. Exercici: utilitzant aquesta directiva crea fitxers
alternatius per les zones amb **un sol bucle** per cada aula.

Com és normal et cal configurar un servidor DNS per verificar que això
funciona.

; ========================================================================
; Fitxer informatica.escoladeltreball.org.zone
; ========================================================================

; Estacions aula N2H.
h01             A       10.200.242.201
h02             A       10.200.242.202
h03             A       10.200.242.203
...
h30             A       10.200.242.230

; Estacions aula N2I. (30 ordinadors)
i01             A       10.200.243.201
i02             A       10.200.243.202
i03             A       10.200.243.203
i04             A       10.200.243.204
...

; Estacions aula N2J. (30 ordinadors)
j01             A       10.200.244.201
j02             A       10.200.244.202
...

; Estacions aula F2A. (34 ordinadors)
a01             A       10.200.246.201
...

; Estacions aula F2G. (34 ordinadors)
g01             A       10.200.245.201
...


; ========================================================================
; Fitxer informatica.escoladeltreball.org.rev.zone 
; ========================================================================

$ORIGIN 242.200.10.in-addr.arpa.
; aula N2H
201             PTR     h01.informatica.escoladeltreball.org.
202             PTR     h02.informatica.escoladeltreball.org.
203             PTR     h03.informatica.escoladeltreball.org.
...
229             PTR     h29.informatica.escoladeltreball.org.
230             PTR     h30.informatica.escoladeltreball.org.

ORIGIN 243.200.10.in-addr.arpa.
; aula N2I
201     	PTR     i01.informatica.escoladeltreball.org.
...
230             PTR     i30.informatica.escoladeltreball.org.

ORIGIN 244.200.10.in-addr.arpa.
; aula N2J
201             PTR     j01.informatica.escoladeltreball.org.
...
232             PTR     j32.informatica.escoladeltreball.org.

ORIGIN 246.200.10.in-addr.arpa.
; aula F2A: 34 ordinadors (prefix 'a')

ORIGIN 245.200.10.in-addr.arpa.
; aula F2G: 34 ordinadors (prefix 'g')

