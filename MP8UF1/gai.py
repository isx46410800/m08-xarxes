# !/usr/bin/python3
# -*-coding: utf-8-*-
#nom autor: Miguel Amorós Moret
#isx: 46410800
#data: 07/1/2019
"Script de python que pasamos dos argumentos(ip/dominio y puerto) y nos muestra su estructura"

import sys
import socket

def error(msg):
    "Muestra un mensaje de error por stderr y acaba"
    print("error: " + msg, file=sys.stderr)
    sys.exit(1)

def report_info(host, port):
    "Crea la estructura de la combinacion de un socket(ip+puerto)"
    "Devuelve una tupla de 5 elementos"    
    
    #Asigna a las variables siguientes, el resultado de la funcion cuando pasamos un ip/dominio y puerto correctos
    #coge el primer elemento de la lista [0]
    (family, type, proto, canonname, sockaddr) = socket.getaddrinfo(host, port)[0]

    #Muestra el resultado de pasarle ip/dominio y un puerto
    print("family:", family)
    print("type:", type)
    print("protocol:", proto)
    print("canonname:", canonname)

    #Miramos la familia si la ip es de ipv4 o ipv6
    if family == socket.AF_INET:
        #Si es ipv4, mostraremos ademas estos campos en sockaddr
        (address, port) = sockaddr
        print("address:", address)
        print("port:", port)
    elif family == socket.AF_INET6:
        #Si es ipv6, mostraremos ademas estos campos en sockaddr
        (address, port, flow_info, scope_id) = sockaddr
        print("address:", address)
        print("port:", port)
        print("flow_info:", flow_info)
        print("scope_id:", scope_id)
    else:
        error("unknown family!")

#Se indica mensaje de error cuando no hay exactamente dos argumentos
if len(sys.argv) != 3:
    error("expected two arguments!")

#La ejecucion que realiza el programa
(host, port) = sys.argv[1:3]
report_info(host, port)

sys.exit(0)

