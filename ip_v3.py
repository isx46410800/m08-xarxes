import sys
from socket import gethostbyaddr as GetHostByAddr
#traduce una ip a address
ip=sys.argv[1]

#asignamos la tupla que da por cada nombre de variable en la funcion gethostbyaddr
(hostname, aliaslist, ipaddrlist) =GetHostByAddr(hostname)

#te retorna una tupla de 3 cosas: hostname,alias,ip
#host=ip[0]
#aliaslist=ip[1]
#ipaddrlist=ip[2][0]

## indicamos la posicion 0 para que nos diga
print("Con print")
print('hostname:',hostname)
print('aliaslist:',aliaslist)
print('ipaddrlist:',ipaddrlist)

#con for
print("Con for")
print(hostname)
#para quitar la lista, recorremos con un for
for element in aliaslist:
	print(element)
	
for element in ipaddrlist:
	print(element)
