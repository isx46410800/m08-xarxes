# Exemple derivat de /usr/lib/systemd/system/named.service

NAMEDCONF=/etc/named.conf
. /etc/sysconfig/named	# pot definir:
#
# OPTIONS="whatever"     --  These additional options will be passed to named
#                            at startup. Don't add -t here, enable proper
#                            -chroot.service unit file.
#
# NAMEDCONF=/etc/named/alternate.conf
#                        --  Don't use -c to change configuration file.
#                            Extend systemd named.service instead or use this
#                            variable.
#
# DISABLE_ZONE_CHECKING  --  By default, service file calls named-checkzone
#                            utility for every zone to ensure all zones are
#                            valid before named starts. If you set this option
#                            to 'yes' then service file doesn't perform those
#                            checks.

# IMPORTANT: codi **no** verificat

case $1 in
	start)
		if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then
			/usr/sbin/named-checkconf -z "$NAMEDCONF"
		else
			echo "Checking of zone files is disabled"
		fi
		/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS
		;;
	reload)
		#MAINPID=$(cat /var/run/named/named.pid)
		MAINPID=$(</var/run/named/named.pid)
		/usr/sbin/rndc reload > /dev/null 2>&1 || /bin/kill -HUP $MAINPID
		;;
	stop)
		#MAINPID=$(cat /var/run/named/named.pid)
		MAINPID=$(</var/run/named/named.pid)
		/usr/sbin/rndc stop > /dev/null 2>&1 || /bin/kill -TERM $MAINPID
		;;
	*) ;; # ERROR
esac
